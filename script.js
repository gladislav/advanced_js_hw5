class Card {
  constructor(title, text, userName, userSurname, userEmail, postId) {
    this.title = title;
    this.text = text;
    this.userName = userName;
    this.userSurname = userSurname;
    this.userEmail = userEmail;
    this.postId = postId;
  }

  createCard() {
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');

    const headerElement = document.createElement('div');
    headerElement.classList.add('card-header');
    headerElement.textContent = this.title;

    const bodyElement = document.createElement('div');
    bodyElement.classList.add('card-body');
    bodyElement.textContent = this.text;

    const footerElement = document.createElement('div');
    footerElement.classList.add('card-footer');
    footerElement.textContent = `${this.userName} ${this.userSurname} (${this.userEmail})`;

    const deleteButton = document.createElement('button');
    deleteButton.classList.add('delete-button');
    deleteButton.textContent = 'Delete';
    deleteButton.addEventListener('click', () => this.deleteCard());

    footerElement.appendChild(deleteButton);

    cardElement.appendChild(headerElement);
    cardElement.appendChild(bodyElement);
    cardElement.appendChild(footerElement);

    return cardElement;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          const cardElement = document.getElementById(`card-${this.postId}`);
          cardElement.remove();
        }
      })
      .catch(error => console.error('Error:', error));
  }
}

function fetchUsersAndPosts() {
  Promise.all([
    fetch('https://ajax.test-danit.com/api/json/users').then(response => response.json()),
    fetch('https://ajax.test-danit.com/api/json/posts').then(response => response.json())
  ])
    .then(([users, posts]) => {
      const newsFeedElement = document.getElementById('newsFeed');

      posts.forEach(post => {
        const { title, body, userId, id } = post;
        const user = users.find(user => user.id === userId);

        if (user) {
          const card = new Card(title, body, user.name, user.surname, user.email, id);
          const cardElement = card.createCard();
          cardElement.id = `card-${id}`;

          newsFeedElement.appendChild(cardElement);
        }
      });
    })
    .catch(error => console.error('Error:', error));
}

fetchUsersAndPosts();